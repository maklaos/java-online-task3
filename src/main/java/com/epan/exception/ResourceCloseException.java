package com.epan.exception;

public class ResourceCloseException extends Exception {
    public ResourceCloseException() {
    }

    public ResourceCloseException(String message) {
        super(message);
    }

    public ResourceCloseException(String message, Throwable cause) {
        super(message, cause);
    }

    public ResourceCloseException(Throwable cause) {
        super(cause);
    }
}
