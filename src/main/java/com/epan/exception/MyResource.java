package com.epan.exception;

public class MyResource implements AutoCloseable {
    @Override
    public void close() throws ResourceCloseException {
        throw new ResourceCloseException("Close exception");
    }

    public void doSomething() {
        System.out.println("Doing something...");
    }
}
