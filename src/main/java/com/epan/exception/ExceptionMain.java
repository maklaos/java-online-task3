package com.epan.exception;

public class ExceptionMain {
    public static void main(String[] args) {
        try (MyResource resource = new MyResource()) {
            resource.doSomething();
        } catch (ResourceCloseException e) {
            e.printStackTrace();
        }
    }
}
