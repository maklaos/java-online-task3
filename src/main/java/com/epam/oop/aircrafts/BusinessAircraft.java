package com.epam.oop.aircrafts;

public class BusinessAircraft extends Aircraft {
    private final int seatsCapacity;

    public BusinessAircraft(String aircraftName, int flightRange, int fuelConsumption, int seatsCapacity) {
        super(aircraftName, flightRange, fuelConsumption);
        this.seatsCapacity = seatsCapacity;
    }

    @Override
    public int getSeatsCapacity() {
        return seatsCapacity;
    }

    @Override
    public int getCargoCapacity() {
        return 0;
    }
}
