package com.epam.oop.aircrafts;

public class CommercialAircraft extends Aircraft {
    private final int cargoCapacity;
    private final int seatsCapacity;

    public CommercialAircraft(String aircraftName, int flightRange, int fuelConsumption, int cargoCapacity, int seatsCapacity) {
        super(aircraftName, flightRange, fuelConsumption);
        this.cargoCapacity = cargoCapacity;
        this.seatsCapacity = seatsCapacity;
    }

    @Override
    public int getSeatsCapacity() {
        return seatsCapacity;
    }

    @Override
    public int getCargoCapacity() {
        return cargoCapacity;
    }
}
