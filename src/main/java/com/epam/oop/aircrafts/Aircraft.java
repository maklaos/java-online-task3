package com.epam.oop.aircrafts;

public abstract class Aircraft {
    private final String aircraftName;
    private final int flightRange;
    private final int fuelConsumption;

    public Aircraft(String aircraftName, int flightRange, int fuelConsumption) {
        this.aircraftName = aircraftName;
        this.flightRange = flightRange;
        this.fuelConsumption = fuelConsumption;
    }

    public abstract int getSeatsCapacity();

    public abstract int getCargoCapacity();

    public int getFlightRange() {
        return flightRange;
    }

    public int getFuelConsumption() {
        return fuelConsumption;
    }

    public String getAircraftName() {
        return aircraftName;
    }

    @Override
    public String toString() {
        return "Aircraft [aircraftName='" + getAircraftName() + "', seatingCapacity=" + getSeatsCapacity()
                + ", cargoCapacity=" + getCargoCapacity() + ", flightRange=" + getFlightRange()
                + ", fuelConsumption=" + getFuelConsumption() + "]";
    }
}
