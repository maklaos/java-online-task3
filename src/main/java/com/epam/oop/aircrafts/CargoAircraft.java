package com.epam.oop.aircrafts;

public class CargoAircraft extends Aircraft {
    private final int cargoCapacity;

    public CargoAircraft(String aircraftName, int flightRange, int fuelConsumption, int cargoCapacity) {
        super(aircraftName, flightRange, fuelConsumption);
        this.cargoCapacity = cargoCapacity;
    }

    @Override
    public int getSeatsCapacity() {
        return 0;
    }

    @Override
    public int getCargoCapacity() {
        return cargoCapacity;
    }
}
