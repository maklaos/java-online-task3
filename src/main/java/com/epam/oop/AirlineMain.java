package com.epam.oop;

import com.epam.oop.manager.Manager;

public class AirlineMain {
    public static void main(String[] args) {
        Manager manager = new Manager();
        manager.start();
    }
}
