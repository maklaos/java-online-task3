package com.epam.oop.airline;

import com.epam.oop.aircrafts.Aircraft;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Airline {
    private String airlineName;
    private List<Aircraft> aircraftList = new ArrayList<>();

    public Airline(String airlineName) {
        this.airlineName = airlineName;
    }

    public void addAircraft(Aircraft aircraft) {
        aircraftList.add(aircraft);
    }

    public List<Aircraft> getAircraftList() {
        return aircraftList;
    }

    public void showTotalCapacity() {
        int totalCargoCapacity = 0;
        int totalSeatsCapacity = 0;
        for (Aircraft aircraft : aircraftList) {
            totalCargoCapacity += aircraft.getCargoCapacity();
            totalSeatsCapacity += aircraft.getSeatsCapacity();
        }
        System.out.println("Total '" + airlineName + "' cargo capacity: " + totalCargoCapacity);
        System.out.println("Total '" + airlineName + "' seats capacity: " + totalSeatsCapacity);
    }

    public List<Aircraft> getAircraftListSortedByFlightRange() {
        return aircraftList
                .stream()
                .sorted(Comparator.comparing(Aircraft::getFlightRange))
                .collect(Collectors.toList());
    }

    public List<Aircraft> filterAircraftByFuelConsumption(int minFuelConsumption, int maxFuelConsumption) throws FuelRangeException {
        if (maxFuelConsumption < minFuelConsumption) {
            throw new FuelRangeException("Min value should be grater than max value");
        }
        return aircraftList
                .stream()
                .filter(aircraft -> (aircraft.getFuelConsumption() >= minFuelConsumption
                        && aircraft.getFuelConsumption() <= maxFuelConsumption))
                .collect(Collectors.toList());
    }
}
