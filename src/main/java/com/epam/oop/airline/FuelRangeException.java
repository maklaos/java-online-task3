package com.epam.oop.airline;

public class FuelRangeException extends Exception {
    public FuelRangeException() {
        super();
    }

    public FuelRangeException(String message) {
        super(message);
    }

    public FuelRangeException(String message, Throwable cause) {
        super(message, cause);
    }

    public FuelRangeException(Throwable cause) {
        super(cause);
    }
}
