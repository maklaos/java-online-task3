package com.epam.oop.manager;

import com.epam.oop.aircrafts.Aircraft;
import com.epam.oop.aircrafts.BusinessAircraft;
import com.epam.oop.aircrafts.CargoAircraft;
import com.epam.oop.aircrafts.CommercialAircraft;
import com.epam.oop.airline.Airline;
import com.epam.oop.airline.FuelRangeException;

import java.util.*;

public class Manager {
    private Airline airline;

    public void start() {
        boolean showCommands = true;
        createAirline();
        while (showCommands) {
            showMenu();
            int userInput = getUserNumberInput();
            if (userInput == 0) {
                showCommands = false;
            } else {
                runCommand(userInput);
            }
        }
    }

    private void createAirline() {
        airline = new Airline("Epam Airlines");
        airline.addAircraft(new CommercialAircraft("Airbus A320", 6100, 3970, 3740, 180));
        airline.addAircraft(new CommercialAircraft("Airbus A318", 5750, 2710, 2120, 130));
        airline.addAircraft(new CommercialAircraft("Boeing 747", 9800, 4500, 17000, 366));
        airline.addAircraft(new CargoAircraft("Antonov An-225", 4000, 7500, 130000));
        airline.addAircraft(new BusinessAircraft("Cessna 172", 1290, 600, 3));
    }

    private void showMenu() {
        System.out.println("\n-- Command list --");
        for (Menu menu : Menu.values()) {
            System.out.println(menu.ordinal() + 1 + " " + menu.getMenuItemName());
        }
        System.out.println("0 Exit");
        System.out.print("Please enter command number:");

    }

    private void runCommand(int menuItemNumber) {
        switch (menuItemNumber) {
            case 1:
                showAircraftList(airline.getAircraftList());
                break;
            case 2:
                showAircraftList(airline.getAircraftListSortedByFlightRange());
                break;
            case 3:
                airline.showTotalCapacity();
                break;
            case 4:
                System.out.print("Enter min fuel consumption:");
                int minFuelConsumption = getUserNumberInput();
                System.out.print("Enter max fuel consumption:");
                int maxFuelConsumption = getUserNumberInput();
                showFilteredAircraftList(minFuelConsumption, maxFuelConsumption);
                break;
            default:
                System.err.println("command not found, try again");
        }
    }

    private int getUserNumberInput() {
        int userInput = -1;
        while (userInput < 0) {
            try {
                Scanner scanner = new Scanner(System.in);
                userInput = scanner.nextInt();
            } catch (InputMismatchException e) {
                System.err.println("Please, enter number:");
            }
        }
        return userInput;
    }

    private void showAircraftList(List<Aircraft> aircraftList) {
        if (aircraftList.isEmpty()) {
            System.out.println("There is no aircraft's in airline");
        } else {
            for (Aircraft aircraft : aircraftList) {
                System.out.println(aircraft.toString());
            }
        }
    }

    private void showFilteredAircraftList(int minFuelConsumption, int maxFuelConsumption) {
        List<Aircraft> aircraftList = new ArrayList<>();
        try {
            aircraftList = airline.filterAircraftByFuelConsumption(minFuelConsumption, maxFuelConsumption);
        } catch (FuelRangeException e) {
            System.err.println(e.getMessage());
        } finally {
            for (Aircraft aircraft : aircraftList) {
                System.out.println(aircraft.toString());
            }
        }
        if (aircraftList.isEmpty()) {
            System.out.println("There is no aircraft's with those parameters");
        }
    }
}
