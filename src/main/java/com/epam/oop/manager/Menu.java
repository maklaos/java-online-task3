package com.epam.oop.manager;

public enum Menu {
    LIST("Show airline aircraft list"),
    SORT("Sort aircraft by flight range"),
    CAPACITY("Show airline cargo and seats capacity"),
    FILTER("Filter aircraft's by fuel consumption");

    private String menuItemName;

    Menu(String menuItemName) {
        this.menuItemName = menuItemName;
    }

    public String getMenuItemName() {
        return menuItemName;
    }
}
